// під кнопкою вивести на сторінку інформацію, отриману з
// останнього запиту – континент, країна, регіон, місто, район??????????.

const findByIp = document.getElementById("find-by-ip");
const app = document.getElementById("app");
findByIp.addEventListener("click", findUserByIp);

async function findUserByIp() {
  app.innerHTML = "";
  let ipResponse = await fetch("https://api.ipify.org/?format=json");
  let ipData = await ipResponse.json();
  let locationResponse = await fetch(
    `http://ip-api.com/json/${ipData.ip}?fields=continent,country,regionName,city,district`
  );
  let locationData = await locationResponse.json();
  console.log(locationData);
  console.log(Boolean(locationData.district));
  const continent = document.createElement("p");
  continent.innerText = "Континент: " + locationData.continent;
  const country = document.createElement("p");
  country.innerText = "Страна: " + locationData.country;
  const region = document.createElement("p");
  region.innerText = "Область: " + locationData.regionName;
  const city = document.createElement("p");
  city.innerText = "Город: " + locationData.city;
  const district = document.createElement("p");
  district.innerText =
    "Район: " + (locationData.district || "Район неизвестен");

  app.append(continent, country, region, city, district);
}
